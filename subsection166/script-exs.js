// exercise 1 - START
let organizeArrays = (array) => {
    // go throught each element of an array

    // add each uniq value to the map, for each uniq value there is special index

    // i have to know for each element 2 things - first is position, second is 
    // amount of elements of this type 
    // in the end i will sort thme by value

    // value (1), amount (4), position (1)


    // firstly - count each element and fill map of data like 1:4, 2:3, 591:1, 392:1
    // then sort it, by keys (save this ordering)
    // now when we know ordering, return amount of this element according to the ordering

    let mapOfUniqValues = new Map();
    array.forEach(item => {
        // check if map has item
        // if does increment value of this key
        // else create key with value 1
        debugger;
        if (hasMapKey(mapOfUniqValues, item)) {
            mapOfUniqValues[item] = Number(mapOfUniqValues[item]) + 1;
        } else {
            mapOfUniqValues[item] = 1;
        }
    })
    // console.log('step 1 is done', mapOfUniqValues);
    // console.log('sorting is done');

    // step 3
    let result = Object.entries(mapOfUniqValues).map(entry => {
        let tempArr = [];
        let key = entry[0];
        let value = entry[1];
        for (let i = 0; i < value; i++) {
            tempArr.push(key);
        }
        return tempArr;
    })

    console.log('Organized', result);
    return result;
}

let divideArray = (array) => {
    let result = [[], []];

    array.forEach(item => {
        if (Number.isInteger(item)) {
            result[0].push(item);
        } else {
            result[1].push(item);
        }
    });

    return result;
}

let hasMapKey = (map, key) => Object.keys(map).includes(`${key}`);

let mySolution = (input) => {
    let numbersAndStrings = divideArray(input);
    console.log('numbersAndStrings', numbersAndStrings);

    let numberArray = numbersAndStrings[0];
    let stringArray = numbersAndStrings[1];
    console.log('numberArray', numberArray);
    console.log('stringArray', stringArray);

    let organizedNumArr = organizeArrays(numberArray);
    let organizedStrArr = organizeArrays(stringArray);

    // let t = organizedNumArr.map(array => {
    //     array.map(item => Number(item))
    // });

    organizedNumArr = organizedNumArr.map(item => {
        if (Array.isArray(item)) {
            return item.map(it => Number(it));
        } else {
            return Number(item);
        }
    });

    let result = [organizedNumArr, organizedStrArr];

    console.log('result', result);
    return result;
}

// let input = [1, 2, 4, 591, 392, 391, 2, 5, 10, 2, 1, 1, 1, 20, 20, '1', '2'];
// let input = [1, 2, 4, 591, 392, 391, 2, 5, 10, 2, 1, 1, 1, 20, 20, '1', '2'];
let input = [1, "2", "3", 2, 1, "2", "3", 2, 1, "2", "3", 2];
mySolution(input);
// exercise 1 - END

// exercise 2 - START
let mySolution = (array, target) => {

    let result

    for (let i = 0; i < array.length; i++) {
        const element = array[i];

        if (target === element) {
            console.log('element === target');
            continue;
        }

        if (element > target) {
            console.log('element > target');
            continue;
        }

        let part2 = target - element;

        if (array.includes(part2)) {
            console.log(`element is ${element}, part2 is ${part2}`)
            result = [element, part2];
            break;
        }

    }

    return result;
}

let answer = mySolution([1, 2, 3, 4, 5, 6 ,7], 13);
console.log(answer);
// exercise 2 - END