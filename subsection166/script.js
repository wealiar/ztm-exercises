// exercise 3 - START
let convertHex2Rgb = (hex) => {
    let r = hex.substr(1, 2);
    let g = hex.substr(3, 2);
    let b = hex.substr(5, 2);

    r = convertWholeHexadecToDec(r);
    g = convertWholeHexadecToDec(g);
    b = convertWholeHexadecToDec(b);

    return `rgb(${r},${g},${b})`;
}

let convertRgb2Hex = (rgb) => {
    let rgbArr = rgb.substring(4, rgb.length - 1).split(',');

    r = convertWholeDecToHexdec(Number(rgbArr[0]));
    g = convertWholeDecToHexdec(Number(rgbArr[1]));
    b = convertWholeDecToHexdec(Number(rgbArr[2]));

    return `#${r}${g}${b}`;
}

let isHexOrRgb = (input) => {
    let rgbRegEx = /^(rgb\((000|0\d{1,2}|1\d\d|2[0-4]\d|25[0-5]),(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5]),(000|0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\))$/;
    let hexRegEx = /^(#([a-fA-F0-9]){6}$)/;
    let indexOfRgbRegEx = input.search(rgbRegEx);
    let indexOfHexRegEx = input.search(hexRegEx);

    if (indexOfRgbRegEx > -1) {
        return 'RGB';
    } else if (indexOfHexRegEx > -1) {
        return 'HEX';
    } else {
        return null;
    }
}

let convert = (input) => {
    console.log(input);
    if (isHexOrRgb(input) === 'HEX') {
        // console.log('it is HEX');
        return convertHex2Rgb(input);
    } else if (isHexOrRgb(input) === 'RGB') {
        // console.log('it is RGB');
        return convertRgb2Hex(input);
    } else {
        console.log('Wrong input format');
        return 'Wrong input format';
    }
}

let convertSingleNumberToHexChar = (number) => {
    switch (number) {
        case 10:
            return 'A';
        case 11:
            return 'B';
        case 12:
            return 'C';
        case 13:
            return 'D';
        case 14:
            return 'E';
        case 15:
            return 'F';
        default:
            return Number(number).toString();
    }
}

let convertSingleHexCharToNumber = (hexChar) => {
    switch (hexChar) {
        case 'A':
            return 10;
        case 'B':
            return 11;
        case 'C':
            return 12;
        case 'D':
            return 13;
        case 'E':
            return 14;
        case 'F':
            return 15;
        default:
            return Number(hexChar);
    }
}

let convertWholeDecToHexdec = (num) => {
    let hexDivider = 16;

    // if (num < hexDivider) return convertSingleNumberToHexChar(num);

    let reminderArr = []
    let quotient = Math.floor(num / hexDivider);

    reminderArr.unshift(convertSingleNumberToHexChar(num % hexDivider));
    reminderArr.unshift(convertSingleNumberToHexChar(quotient % hexDivider));
    return reminderArr.reduce((acc, item) => acc = acc + item);
}

let convertWholeHexadecToDec = (hex) => {
    // if (hex.length === 1) return convertSingleHexCharToNumber(hex);

    let result = 0;
    let hexMultiplier = 16;

    let hexLength = hex.length;
    for (let i = 0; i < hex.length; i++) {
        const elem = convertSingleHexCharToNumber(hex[i]);
        result += elem * Math.pow(hexMultiplier, hexLength - 1 - i);
    }

    return result;
}

let hexInput = '#FF0000'; // red hex
let rgbInput = 'rgb(255,0,0)'; // red rgb

let answer = convert(hexInput); // rgb(255,0,0)
console.log(answer);

let answer2 = convert(rgbInput); // #FF0000
console.log(answer2);
// exercise 3 - END